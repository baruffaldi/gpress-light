import os
import webapp2
from django.template.loader import render_to_string as render

class SiteMap(webapp2.RequestHandler):
    def gettemplates(self, a, path, files):
        self.response.out.write('<dl>')
        if files:
            baseurl = '/'.join(path.split("/")[2:])
            self.response.out.write('<dt><h2>%s</h2></dt>' % baseurl.replace('/', ' ').replace('\\', ' '))
            for page in files:
                if page.split('.')[-1] in ['html','htm']:
                    pagename = ".".join(page.split('.')[:1])
                    url = "%s/%s" % (baseurl, pagename)
                    self.response.out.write('<dd><a href="%s" title="%s">%s</a></dd>' % (url, pagename, pagename))
        self.response.out.write('</dl>')

    def get(self):
        self.response.out.write('<h1>Sitemap</h1>')
        self.response.out.write('<h2>main</h2>')
        self.response.out.write(os.path.walk('./templates/', self.gettemplates, None))
        self.response.out.write('<hr /><small>GPress Express on <i>%s</i> port %s</small>' % (os.environ['HTTP_HOST'].split(':')[0], os.environ['SERVER_PORT']))

class SiteMapXml(webapp2.RequestHandler):
    def gettemplates(self, a, path, files):
        # Recover pages
        self.response.out.write('')

    def get(self):
        # Build the XML and send it to the browser
        self.response.out.write('')

class MainPage(webapp2.RequestHandler):
    def notfound(self):
        self.response.out.write('<h1>404 - <i>Page not found</i>.</h1><p>Try searching on <a href="/gpe-sitemap" title="Sitemap">this page</a>.</p>')
        self.response.out.write("""<hr /><pre>    It is said, "To err is human"
    That quote from alt.times.lore,
    Alas, you have made an error,
    So I say, "404."

    Double-check your URL,
    As we all have heard before.
    You ask for an invalid filename,
    And I respond, "404."

    Perhaps you made a typo --
    Your fingers may be sore --
    But until you type it right,
    You'll only get 404.

    Maybe you followed a bad link,
    Surfing a foreign shore;
    You'll just have to tell that author
    About this 404.

    I'm just a lowly server
    (Who likes to speak in metaphor),
    So for a request that Idon't know,
    I must return 404.

    Be glad I'm not an old mainframe
    That might just dump its core,
    Because then you'd get a ten-meg file
    Instead of this 404.

    I really would like to help you,
    But I don't know what you're looking for,
    And since I don't know what you want,
    I give you 404.

    Remember Poe, insane with longing
    For his tragically lost Lenore.
    Instead, you quest for files.
    Quoth the Raven, "404!"
        </pre>""")
        self.response.out.write('<hr /><small>GPress Express on <i>%s</i> port %s</small>' % (os.environ['HTTP_HOST'].split(':')[0], os.environ['SERVER_PORT']))

    def check_path(self, path):
        if not os.path.exists(path):
            if os.path.exists(path+'.htm'):
                path = path+'.htm'
        if not os.path.exists(path):
            if os.path.exists(path+'.html'):
                path = path+'.html'
        return path

    def get(self):
        reqpath = '%s/templates%s' % (os.path.curdir, self.request.path)

        reqpath = self.check_path(reqpath)

        if reqpath.endswith('/'):
            reqpath = reqpath + 'index'

        reqpath = self.check_path(reqpath)

        if not os.path.exists(reqpath):
            self.error(404)
            self.notfound()
            return

        self.response.out.write(render(reqpath, {}))

Debug = os.environ["SERVER_SOFTWARE"].split("/")[0].lower() == "development"

application = webapp2.WSGIApplication([
                  ('/gpe-sitemap', SiteMap),
                  ('/sitemap.xml', SiteMapXml),
                  ('.*', MainPage)],
                  debug=Debug)
